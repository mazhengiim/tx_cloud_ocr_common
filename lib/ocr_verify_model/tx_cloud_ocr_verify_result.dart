import 'dart:convert';

TxCloudOcrVerifyResult txCloudOcrVerifyResultFromJson(String str) =>
    TxCloudOcrVerifyResult.fromJson(json.decode(str));

String txCloudOcrVerifyResultToJson(TxCloudOcrVerifyResult data) =>
    json.encode(data.toJson());

class TxCloudOcrVerifyResult {
  TxCloudOcrVerifyResult({
    this.result = false,
    this.message = '',
    // this.frontFullImageSrc = '',
    // this.backFullImageSrc = '',
    this.idcard = '',
    this.name = '',
    this.sex = '',
    this.nation = '',
    this.address = '',
    this.birth = '',
  });

  bool result;
  String message;
  // String frontFullImageSrc;
  // String backFullImageSrc;
  String idcard;
  String name;
  String sex;
  String nation;
  String address;
  String birth;

  factory TxCloudOcrVerifyResult.fromJson(Map<String, dynamic> json) =>
      TxCloudOcrVerifyResult(
        result: json["result"],
        message: json["message"],
        idcard: json["idcard"] ?? "",
        name: json["name"] ?? "",
        sex: json["sex"] ?? "",
        nation: json["nation"] ?? "",
        address: json["address"] ?? "",
        birth: json["birth"] ?? "",
      );

  Map<String, dynamic> toJson() => {
    "result": result,
    "message": message,
    "idcard": idcard,
    "name": name,
    "sex": sex,
    "nation": nation,
    "address": address,
    "birth": birth,
  };
}